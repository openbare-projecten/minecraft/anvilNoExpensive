package nl.robertvankammen.anvilnoexpensive;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.InventoryView;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onAnvil(PrepareAnvilEvent event) {
        if (event.getInventory().getRepairCost() > 39) {
            event.getView().setProperty(InventoryView.Property.REPAIR_COST, 39);
            event.getInventory().setMaximumRepairCost(1000);
            event.getInventory().setRepairCost(39);
        }
    }
}
